<?php

namespace App\Providers;

use App\Domain\Category\Events\CategoriesPrepared;
use App\Domain\Category\Listeners\SanitizeCategories;
use App\Domain\Csv\Events\RecordsPrepared;
use App\Domain\Product\Listeners\ImportRecords as ProductImporter;
use App\Domain\Elasticsearch\Listeners\ImportRecords as ElasticImporter;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        RecordsPrepared::class => [
            SanitizeCategories::class
        ],

        CategoriesPrepared::class => [
            ProductImporter::class,
            ElasticImporter::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
