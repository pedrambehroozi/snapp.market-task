<?php

declare(strict_types = 1);

namespace App\Domain\Shared\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;

interface Fetcher
{
    public function all($category_id = null): LengthAwarePaginator;
}