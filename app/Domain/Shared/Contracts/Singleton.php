<?php

declare(strict_types = 1);

namespace App\Domain\Shared\Contracts;

abstract class Singleton
{
    private static array $instances = [];

    public static function getInstance(): self
    {
        $class = static::class;

        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new static;
        }

        return self::$instances[$class];
    }
}