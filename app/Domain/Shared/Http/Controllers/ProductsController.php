<?php

declare(strict_types = 1);

namespace App\Domain\Shared\Http\Controllers;

use App\Domain\Shared\Contracts\Fetcher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductsController
{
    protected Fetcher $fetcher;

    public function __construct(Fetcher $fetcher)
    {
        $this->fetcher = $fetcher;
    }

    public function all(Request $request): JsonResponse
    {
        $category_id = $request->input('category_id');

        $products = $this->fetcher->all($category_id);

        return response()->json($products->toArray());
    }
}