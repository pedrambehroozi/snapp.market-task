<?php

declare(strict_types = 1);

namespace App\Domain\Category;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Model extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $fillable = ['name'];

    public function products(): HasMany
    {
        return $this->hasMany('App\Domain\Product\Model');
    }

    public function getIdByName(string $name): int
    {
        $category = $this->firstOrCreate([
            'name' => $name
        ]);

        return $category->id;
    }

    public function getDefaultCategoryId(): int
    {
        return $this->getIdByName('default');
    }
}