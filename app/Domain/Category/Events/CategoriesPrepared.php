<?php

declare(strict_types = 1);

namespace App\Domain\Category\Events;

class CategoriesPrepared
{
    protected array $records;

    public function __construct(array $records)
    {
        $this->records = $records;
    }

    public function getRecords(): array
    {
        return $this->records;
    }
}