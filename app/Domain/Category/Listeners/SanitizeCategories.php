<?php

namespace App\Domain\Category\Listeners;

use App\Domain\Category\Events\CategoriesPrepared;
use App\Domain\Csv\Events\RecordsPrepared;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Domain\Category\Model as Category;

class SanitizeCategories implements ShouldQueue
{
    protected Category $model;

    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    protected function sanitize(array $record): array
    {
        if (!isset($record['category']) || is_null($record['category'])) {
            $record['category_id'] = $this->model->getDefaultCategoryId();
        } else if (is_string($record['category'])) {
            $record['category_id'] = $this->model->getIdByName($record['category']);
        }

        unset($record['category']);

        return $record;
    }

    /**
     * Handle the event.
     */
    public function handle(RecordsPrepared $event): void
    {
        $records = $event->getRecords();

        foreach ($records as $key => $record) {
            $records[$key] = $this->sanitize($record);
        }

        event(new CategoriesPrepared($records));
    }
}
