<?php

declare(strict_types = 1);

namespace App\Domain\Product\Listeners;

use App\Domain\Category\Events\CategoriesPrepared;
use App\Domain\Product\Model as Product;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImportRecords implements ShouldQueue
{
    protected Product $model;

    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function handle(CategoriesPrepared $event)
    {
        $records = $event->getRecords();

        $this->model->fromArray($records);
    }
}