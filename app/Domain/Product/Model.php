<?php

declare(strict_types = 1);

namespace App\Domain\Product;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Model extends BaseModel
{
    const CACHE_KEY = "all_products_list";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';
    
    protected $fillable = [
        'name', 'price', 'description', 'quantity',
        'category_id'
    ];

    protected $casts = [
        'quantity' => 'integer',
        'price' => 'integer',
        'category_id' => 'integer'
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo('App\Domain\Category\Model');
    }

    public function fromArray(array $records): void
    {
        $now = Carbon::now()->toDateTimeString();

        $records = array_map(function ($record) use ($now) {
            return array_merge($record, [
                'created_at' => $now,
                'updated_at' => $now
            ]);
        }, $records);

        try {
            DB::beginTransaction();
            
            $this->insert($records);
            $this->clearCache();

            DB::commit();
        }
        catch (\Exception $exception) {
            DB::rollBack();

            throw new \Exception($exception->getMessage());
        }
    }

    public function scopeOfCategory(Builder $query, int $category_id): Builder
    {
        return $query->where('category_id', $category_id);
    }

    public static function getAll($category_id = null)
    {
        return Cache::remember(self::CACHE_KEY, 60 * 60, function () use ($category_id) {
            $query = self::select('id', 'name', 'price', 'quantity', 'description', 'category_id')
                ->with('category:id,name');

            if ($category_id) {
                $query->ofCategory($category_id);
            }

            return $query->paginate(10);
        });
    }

    public function clearCache(): void
    {
        Cache::forget(self::CACHE_KEY);
    }
}
