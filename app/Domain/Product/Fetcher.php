<?php

declare(strict_types = 1);

namespace App\Domain\Product;

use App\Domain\Product\Model as Product;
use App\Domain\Shared\Contracts\Fetcher as BaseFetcher;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class Fetcher implements BaseFetcher
{
    public function all($category_id = null): LengthAwarePaginator
    {
        return Product::getAll($category_id);
    }
}