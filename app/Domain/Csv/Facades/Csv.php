<?php

declare(strict_types = 1);

namespace App\Domain\Csv\Facades;

/**
 * @method static \App\Domain\Csv\Service setFilePath(string $filePath)
 * @method static void process()
 * @method \League\Csv\Stream getCsv()
 */

use Illuminate\Support\Facades\Facade;

class Csv extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'csv';
    }
}