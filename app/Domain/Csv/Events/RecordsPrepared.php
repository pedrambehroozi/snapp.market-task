<?php

declare(strict_types = 1);

namespace App\Domain\Csv\Events;

class RecordsPrepared
{
    protected array $records;

    public function __construct(array $records)
    {
        $this->records = $records;
    }

    public function getRecords(): array
    {
        return $this->records;
    }
}