<?php

declare(strict_types = 1);

namespace App\Domain\Csv\Http;

use App\Domain\Csv\Http\Request as CsvRequest;
use App\Domain\Csv\Facades\Csv;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller as ‌BaseController;

class Controller extends ‌BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function import(CsvRequest $request): JsonResponse
    {
        if ($request->file('products')->getClientOriginalExtension() !== 'csv') {
            return response()->json([
                'error' => 'Please select a valid \'csv\' file'
            ])->setStatusCode(422);
        }

        $path = $request->file('products')->storeAs('products', 'products_'.sha1((string)time()).'.csv');

        Csv::setFilePath($path)
            ->process();

        return response()->json([
            'message' => 'Successfully uploaded the file.'
        ])->setStatusCode(200);
    }
}
