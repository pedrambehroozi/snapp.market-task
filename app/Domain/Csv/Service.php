<?php

declare(strict_types = 1);

namespace App\Domain\Csv;

use App\Domain\Csv\Events\RecordsPrepared;
use League\Csv\Reader;

class Service
{
    protected string $filePath;

    protected ?Reader $csv = null;

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function setReader(Reader $reader): self
    {
        $this->csv = $reader;

        return $this;
    }

    public function getCsv(): Reader
    {
        if (!$this->csv) {
            $this->csv = Reader::createFromPath(storage_path('app/'.$this->filePath), 'r');
            $this->csv->setHeaderOffset(0);
        }

        return $this->csv;
    }

    public function process(): void
    {
        $records = $this->getRecords();

        event(new RecordsPrepared($records));
    }

    protected function getRecords(): array
    {
        return iterator_to_array($this->getCsv()->getRecords($this->getCsv()->getHeader()));
    }

    
}