<?php

declare(strict_types = 1);

namespace App\Domain\Elasticsearch;

use App\Domain\Category\Model as Category;
use App\Domain\Shared\Contracts\Singleton;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class Facade extends Singleton
{
    protected ?Client $client = null;

    public static function build(): self
    {
        $instance = self::getInstance();

        if (is_null($instance->client)) {
            $instance->client = ClientBuilder::create()
                ->setHosts(['sm-elastic:9200'])
                ->build();
        }

        return $instance;
    }

    public static function index(array $records): void
    {
        $instance = self::getInstance();

        $params = [];

        foreach ($records as $record) {
            $category = Category::find($record['category_id']);

            $record['category'] = [
                'id' => $category->id,
                'name' => $category->name
            ];

            $params['body'][] = [
                'index' => [
                    '_index' => 'products'
                ]
            ];

            $params['body'][] = [
                $record
            ];
        }

        $instance->client->bulk($params);
    }

    public static function fetch($category_id = null, $page = 1): array
    {
        $instance = self::getInstance();
        $per_page = config('products.per_page');

        $params = [
            'index' => 'products',
            'from' => ($page - 1) * $per_page,
            'size' => $per_page
        ];

        if (!is_null($category_id)) {
            $params['body'] = [
                'query' => [
                    'match' => [
                        "category_id" => $category_id
                    ]
                ]
            ];
        }

        $result = $instance->client->search($params);

        $data = array_map(function ($record) {
            return $record['_source'];
        }, $result['hits']['hits']);

        return [
            'data' => $data,
            'total' => $result['hits']['total']['value']
        ];
    }
}