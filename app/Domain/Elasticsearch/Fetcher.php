<?php

declare(strict_types = 1);

namespace App\Domain\Elasticsearch;

use App\Domain\Shared\Contracts\Fetcher as BaseFetcher;
use App\Domain\Elasticsearch\Facade as Elastic;
use Illuminate\Pagination\LengthAwarePaginator;

class Fetcher implements BaseFetcher
{
    public function all($category_id = null): LengthAwarePaginator
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $results = Elastic::build()
            ->fetch($category_id, $currentPage);

        $paginator = new LengthAwarePaginator(
            $results['data'],
            $results['total'],
            config('products.per_page'),
            $currentPage,
            ['path' => route('products')]
        );

        return $paginator;
    }
}