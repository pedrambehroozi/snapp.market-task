<?php

declare(strict_types = 1);

namespace App\Domain\Elasticsearch\Listeners;

use App\Domain\Category\Events\CategoriesPrepared;
use App\Domain\Elasticsearch\Facade as Elasticsearch;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImportRecords implements ShouldQueue
{
    public function handle(CategoriesPrepared $event)
    {
        $records = $event->getRecords();

        Elasticsearch::build()
            ->index($records);
    }
}