<?php

return [
    'fetcher' => 'App\\Domain\\'.env("PRODUCTS_FETCHER", "Product").'\\Fetcher',

    'per_page' => 10
];