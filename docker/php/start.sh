#!/bin/bash

set -e

role=${CONTAINER_ROLE:-app}

if [ "$role" = "app" ]; then

    exec php-fpm

elif [ "$role" = "queue" ]; then

    echo "Running queue..."
    php /var/www/snapp.market/artisan queue:work --verbose --tries=3 --timeout=90

elif [ "$role" = "scheduler" ]; then

    while [ true ]
    do
        php /var/www/snapp.market/artisan schedule:run --verbose --no-interaction &
        sleep 60
    done

else

    echo "Role not found"
    exit 1

fi