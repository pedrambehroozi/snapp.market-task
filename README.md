# Snapp.market Test Project
This is the implementation of the test project provided by snapp.market.

## Prerequisites
Install [docker](https://www.docker.com/) daemon. Everything else will be installed following [Installation](#Installation) section.

## Installation
1. Clone this repository.
2. Copy `.env.example` to `.env`. All necessary settings are pre-configured in the file. You can change any value you want, **except**:

    ```env
    APP_URL=http://snappmarket.local
    DB_HOST=sm-database
    DB_PORT=3306
    REDIS_HOST=sm-redis
    ```

3. The **Nginx** container will use port **80**, so make sure other programs which are run on this port are stopped.
4. Run `docker-compose up -d` to bring up the containers.
5. Add `snappmarket.local` to your hosts file:

    **Linux**
    ```bash
    sudo echo "127.0.0.1 snappmarket.local" >> /etc/hosts
    ```
    
    Replace `127.0.0.1` with the IP of the machine you're installing this project on.
    
6. Install the dependencies:

    ```bash
    docker exec -it sm-php composer install
    ```

7. Migrate the database:

    ```bash
    docker exec -it sm-php php artisan migrate
    ```

8. Seed the database:

    ```bash
    docker exec -it sm-php php artisan db:seed
    ```

    This will add a user with these information:

    ```json
    {
        "name": "Admin",
        "email": "admin@snapp.market",
        "password": "AStrongPassword",
        "role": "admin"
    }
    ```

9. Generate JWT secret:

    ```bash
    docker exec -it sm-php php artisan jwt:secret
    ```

## The Endpoints

There are 3 endpoints in this implementation. Follow this link to [visit a **Postman** generated document](https://documenter.getpostman.com/view/6502283/SztHXQp6).

## The CSV File

Any valid csv file can be uploaded. Note the header **MUST** be the first line of the file and have these keys. The order of the keys is not important:

```
name
category
price
quantity
description
```

## Fetchers

You can fetch products from database or from Elasticsearch engine. Change `PRODUCTS_FETCHER` variable from the environment.

To use database fetching:
```env
PRODUCTS_FETCHER=Product
```
To use Elasticsearch:
```env
PRODUCTS_FETCHER=Elasticsearch
```

**NOTE**

If the disk where this project is stored on has less than 10% free space, Elasticsearch indexing will not work and throws this error:

```
index [products] blocked by: [TOO_MANY_REQUESTS/12/index read-only / allow delete (api)];
```

In this case just make sure you have more than 10% free disk space, and reupload the csv file.