<?php

use App\Domain\Elasticsearch\ElasticFetcher;
use App\Domain\Shared\Http\Controllers\ProductsController;
use App\Domain\Shared\Http\Resources\ProductsCollection;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
});

Route::middleware('auth:api')
    ->namespace('\App\Domain\Csv\Http')
    ->prefix('csv')
    ->group(function() {
        Route::post('import', 'Controller@import');
    });

Route::group(['prefix' => 'products'], function() {
    Route::get('/', function (Request $request) {
        $fetcher_class = config('products.fetcher');

        $fetcher = new $fetcher_class();
        $controller = new ProductsController($fetcher);

        return $controller->all($request);
    })->name('products');
});